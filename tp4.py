import time , os


def menu():
    opc = int(input("💩💩💩Ingrese una opción💩💩💩\n"+ 
                    "1-Registrar productos\n"+
                    "2-mostrar el listado de productos\n"+
                    "3-Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta]\n"+
                    "4-le sume X al stock de todos los productos menor al valor Y \n"+
                    "5-eliminar todos los productos cuyo stock sea igual a cero \n"+
                    "6-Salir\n"+
                    "Ingrese una opción: "))
    while not(opc>=1 and opc<=6):
        opc = int(input("Ingrese una opción valida [1-6]; "))
    os.system("Cls")
    return opc

def presioneTecla():
    input("Ingrese una tecla")

def xValidacion():
    x = int(input("Ingrese cuanto le desea sumar: "))
    while not(x>0):
        print("Que sea positivo el numero a sumar")
        x = int(input("Ingrese cuanto le desea sumar: "))
    return x

def precioValidacion():
    precio = float(input("Ingrese precio: $"))
    while not(precio>0):
        precio = float(input("Ingrese precio mayor a 0🤡: $"))
    return precio


def stockValidacion():
    stock = int(input("Ingrese stock: "))
    while not(stock>0):
        stock = int(input("Ingrese stock valido 🤡: "))
    return stock

def yValidacion():
    y = int(input("Ingrese un valor: "))
    while not(y>=0):
        y = int(input("Ingrese valor mayor o igual a 0  🤡: "))
    return y

def ingresarDatosDic():
    dicc = {}
    cantidad = int(input("Ingrese la cantidad de productos que desea: "))
    for i in range(cantidad):
        codigo = int(input("Ingrese codigo: "))
        if codigo not in  dicc:
           descripcion = input("Ingrese descripción: ")
           precio = float(input("Ingrese el precio: $"))
           stock = int(input("Ingrese stock: "))
           dicc[codigo] = [precio,descripcion,stock]
           print("Producto Agregado")
        else:
            print("El producto ya existe")
    return dicc

def mostrarDick(diccionario):
    for clave , valor in diccionario.items():
        print(f"la clave es {clave} y el valor es {valor}")

def intervaloDA():
    print("Intervalos 😋😋")
    desde = int(input("Ingrese donde quiere comenzar: "))
    while not(desde >=0):
        print("Mayor a cero 🤪")
        desde = int(input("Ingrese donde quiere comenzar: "))
    hasta = int(input("Ingrese donde quiere terminar: "))
    while not(hasta >=0 and hasta >desde):
        print("Tiene que ser mayor a 0 y mayor a desde")
        desde = int(input("Ingrese donde quiere comenzar: "))
    return (desde ,hasta)

def mostrarIntervalo(diccionario):
    desde , hasta = intervaloDA()
    print("los productos que se encuentran en el intervalo [",desde,",",hasta,"]")
    for clave ,valor in diccionario.items():
        if (valor[2] >=desde and  valor[2] <= hasta):
            print(clave,valor)

def operacion(diccionario):
    x = xValidacion()
    y = yValidacion()
    for clave,valor in diccionario.items():
        if (valor[2]<y):
            new = valor[2]+x
            diccionario [clave] = [valor[0],valor[1],new]
        else:
            print("NO ES MENOR AL VALOR DE Y")

def eliminar(diccionario):
    for item in list(diccionario.keys()):
        if (diccionario[item][2]==0):
            del diccionario[item]


diccionarioCargado = False
opcion = 0
while opcion != 6:
    opcion  = menu()
    if opcion == 1:
        diccionario = ingresarDatosDic()
        diccionarioCargado = True
    elif opcion == 2:
        if diccionarioCargado:
            mostrarDick(diccionario)
        else:
            print("Debe cargar el diccionario 👾👾")
        presioneTecla()
    elif opcion == 3:
        if diccionarioCargado:
            mostrarIntervalo(diccionario)
        else:
            print("Debe cargar el diccionario 👾👾")
        presioneTecla()
    elif opcion == 4:
        if diccionarioCargado:
            operacion(diccionario)
        else:
            print("Debe cargar el diccionario 👾👾")
        presioneTecla()
    elif opcion == 5:
        if diccionarioCargado:
            eliminar(diccionario)
        else:
            print("Debe cargar el diccionario 👾👾")
        presioneTecla()
    elif opcion == 6:
        print("Error 666🔥🔥Error 666🔥🔥Error 666🔥🔥Error 666🔥🔥Error 666🔥🔥Error 666🔥🔥Error 666🔥🔥Error 666🔥🔥Error 666🔥🔥Error 666🔥🔥")

            


            


