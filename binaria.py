def busquedaBinaria(lista, x):
    """Búsqueda binaria
    Precondición: lista está ordenada
    Devuelve -1 si x no está en lista;
    Devuelve p tal que lista[p] == x, si x está en lista
    """

    izq = 0 # izq guarda el índice inicio del segmento
    der = len(lista) -1 # der guarda el índice fin del segmento

    # un segmento es vacío cuando izq > der:
    while izq <= der:
        medio = (izq+der)//2

        print ("DEBUG:", "izq:", izq, "der:", der, "medio:", medio)

        if lista[medio] == x:
            return medio
        elif lista[medio] > x:
            der = medio-1
        else:
            izq = medio+1
    return -1


def main():
    #lista = [1, 3, 5, 12, 30, 31, 35, 60, 61, 63, 68,70,90]
    lista = range(100000)
    x = int(input("¿Valor buscado?: "))
    resultado = busquedaBinaria(lista, x)
    print ("Resultado:", resultado)

# Programa principal 🤪
main()




import time
lista01 =[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
lista02 =[9,10,11,5,12,13,14,15]

def promedio(lista01):
    suma = 0
    for item in lista01:
        suma +=item
    return suma/len(lista01)

def minimo(lista02):
    min = lista02[0]
    for i,item in enumerate (lista02):
        if item < min :
            min = item
    return min

def factorial(lista02,minimo):
    fact = 1
    minimo = minimo(lista02)
    for i in range(1,minimo+1):
        fact = fact*i
    return fact

def sumarMinyFact(lista01,lista02,minimo):
    promedioo = promedio(lista01)
    factmin = factorial(lista02,minimo)
    for i,item in enumerate(lista01):
        if item < promedioo:
            lista01[i] = lista01[i]+factmin
    print(lista01)


def multiplodeTres(lista02):
    listaAux = list(lista02)
    for i,valor in enumerate(listaAux):
        if valor % 3 ==0:
            lista02.remove(valor)
    print(lista02)


print(promedio(lista01))
time.sleep(2)
print(minimo(lista02))
time.sleep(2)
multiplodeTres(lista02)
time.sleep(2)
print(factorial(lista02,minimo))
time.sleep(2)
sumarMinyFact(lista01,lista02,minimo)